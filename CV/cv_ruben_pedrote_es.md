# Curriculum Vitae - Darth Nihilus

>**s̸̛̪̩̫̟͔͚̳̖͓̞̝̈̂̌̓̏̉͗̚ͅd̵̺̤͚͔̗͇͕͖̘̤̀͒̈́͋̎̌́̓̍̒̋̚͠͠j̶̺͂̃̌͗̀̃̅̕͝͠͠f̸̝̫̙̦̞̖̹̏̿s̸̨̧̛̼͉͈̫̞̳͙̹͈̝̪̠͍͒̓̐̿̄̀͐̌̒̚̕ḑ̴̼̯̲̼̩̞̺̳͓̫̜̠̱̓̇̒͒̑̑͒͒͒̚͠͠k̵̳͖̤̤̻͛̾͊ǰ̵̡̛̛̫̬͚͔͂͑̾͆͌̔̐́͘c̴̛͖̓̓̄͛̾̈́́̆͆̑̀̓̆͠b̶̭̫̪̓̅͋̚͝å̵̡̭̟̳̖̳̘͕͐̈̎̎̌̃c̴̞̼̘͎͙̮͇̜͛́̇̚͝b̷̙̺͈̩̤͑͌́͒͋̔ǹ̴̰̫̼̦̺ŝ̶͕̥̣̫̪͈̞̩͕͍̱͂̈́́ą̷̤̱͔̖̙̠͒̓̾̊̽͌̆̒̏́̕d̷̡̪̹̩̻̚j̶̧̢̛̙̗̻̜̱̽̿̊̔̂̂̋k̵̢͙͎̹̟̭͈̻̣̳̽͂͗̾̓̈̂̈́̏̔͠f̸͈̝̯̰̖̰̰̮͍̙̫̱̋̎͆͒̽̉̇͋̽͊̈́̒̔͌͜͝b̷̛̪̠͉͔͂̇͠ạ̷̿͗̅̉̈͊̇s̸̥͉̻̤̺̳͔̣͕̥̈́̄k̶̡̳̣̼̫͔͇̖̖̱̖̮͚͌̿̓́̈́͒́̔h̴͎͉̏̇̿̒̅͆̅̐͘̚͘ͅa̷̯̳̩͓͍͊͆́̒̄͋͝͝d̵͈͎̙̤̲̩̱͓͕̝̈̑̈́̋̊͜n̴̨͔̼̏̃̿̔̾͘͘ơ̸̡̨͙̼̬͎̱̈́̔̋̑͗́͂͂̽͂̄̽s̸̏̀̿**

![alt text](img/DarthNihilus.jpg)

Antes era un soldado y ahora tengo hambre.

Si no sabes quién soy: [Link a mi biografía](https://starwars.fandom.com/es/wiki/Darth_Nihilus)

#

# Experiencia profesional


* Soldado de la República Galáctica
   * Guerras Mandalorianas
        * Malachor V

* Lord Sith
    * Triunvirato Sith

# Títulos académicos

1. Sí.


# Habilidades


|   Habilidad    |   Descripción   | Experiencia |
|----------------|:---------------:|------------:|
|Sable de luz    | Habilidad de combate con sable de luz  | ⭐⭐⭐⭐ |
|Hambre          | Capacidad de alimentarse de la muerte de los seres vivos        | ⭐⭐⭐⭐⭐ |
|Cortar la fuerza| Exiliar a un ser vivo de su conexión con la fuerza | ⭐⭐⭐⭐ |
|Alquimia Sith   | Ciencia que abarca cualquier poder o técnica que utilice el lado oscuro para alterar permanentemente un objeto o ser vivo | ⭐⭐⭐ |


# Tarjeta de visita
Puedes utilizar este JSON para guardar mis datos

```JSON
{
    "name" : "Darth Nihilus",
    "title" : "Lord of Hunger",
    "ship" : "Devastator"
}
```